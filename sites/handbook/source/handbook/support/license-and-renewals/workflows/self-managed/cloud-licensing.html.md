---
layout: markdown_page
title: Cloud licensing support exemption
description: "How to generate a legacy license for a customer who cannot use Cloud Licensing"
category: GitLab Self-Managed licenses
---

{:.no_toc}

----

## Overview
Beginning in April 2022, in order to further encourage Cloud Licensing, customers will no longer receive a license file attached to their activation email. Instead, they will only receive a Cloud License activation code.

Customers who cannot activate with Cloud Licensing will need to obtain a legacy license file. The process for this is that the customer will need to contact their sales account manager to request such an exemption.


## Direct Customers
Customers who are not purchasing through a channel partner will be able to access their license file from within Customer Portal.


## Channel Customers
Channel customers do not have access to the Customer Portal. Instead, they will be directed to submit a Support ticket to receive a license file.

The customer's license can be accessed via CustomerDot in the licenses section. The license should then be emailed to the customer directly after creation. Unless the circumstances require, we should not send the licenses to any GitLab employee. Exceptions may include air-gapped installs or other situations where the customer cannot receive the license via the email.

Note that a license should always be generated with the end-user email address. A license should not be generated with reseller or GitLab team member details if the license is for a customer.
